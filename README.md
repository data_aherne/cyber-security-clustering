<hr>
<h5 >
  Author:  Derek Aherne , Date: 2022-02-17
</h5>
</div>

<hr>

## Context

Cybersecurity firms are constantly innovating new ways to recognize threats from information
being collected from log files through services such as Splunk. Splunk is a log aggregation system which collects
information on an enterprise network such as logins, login failures, requests and the origins of the requests, etc. Unsupervised machine learning has 
potential to be a useful tool in identifying malware anomolies and patterns, below is an example of such a use case.

## Objective

A dataset is unlabeled and contains 310 columns of mixed data types including numerical, strings, and
timestamps. It is suspected that approximately 0.1% of the records are malicious. The object is to write a function that returns a list of values in the 'id' column predicted as malicious/anomalies.

## Data preparation 

The data contains a mix of data types and nested values. The following data preprocessing steps were conducted:

* strings containing JSON were processed to have only one quotation mark to produce a valid JSON string
* nested JSON/dictionaries were flattened and appended
* duplicated column names were renamed
* any columns duplicated by their values were dropped (with one column retained)
* columns with only one unique value were dropped, removing columns with exactly zero variance
* columns containing only empty JSON and NaN values, or a mix of the two, were dropped

## Feature engineering

The following feature engineering steps were carried out:

* categorical variables with more than 50 values are not used and were dropped. This was a manual trial and error process to determine the best threshold - ideally this would be a step within a pipeline that could be optimised
* categorical variables were converted into dummy variables
* the data was normalized using z score transformations
* Principal Component Analysis(PCA) was used to reduce the dimensionality of the data. The optimal number of components was determined to be 300 components using the distortion technique. This was tested and it was found that the class distribution remained approximately the same.

## Modelling

The scikit-learn API was used to build and evaluate the models. As the malware inidicator is intended as a binary classifier the models chosen were the clustering algorithms that allow a user input of k. In this model we want to fix that number of cluster as k=2. The chosen models are listed below:

* K-means
* agglomerative clustering
	* Ward linkage
	* single linkage
	* average linkage
	* complete linkage
* Self-Organizing Maps (SOM)

### K-means

The K-Means algorithm clusters data by trying to separate samples in k groups of equal variance, minimizing a criterion known as the inertia or within-cluster sum-of-squares. This algorithm requires the number of clusters to be specified. It scales well to large number of samples and has been used across a large range of application areas in many different fields

source: **https://scikit-learn.org/stable/modules/clustering.html#k-means**

### Agglomerative clustering

Hierarchical clustering is a general family of clustering algorithms that build nested clusters by merging or splitting them successively. 
The linkage criteria determines the metric used for the merge strategy:

* Ward minimizes the sum of squared differences within all clusters. It is a variance-minimizing approach and is similar to the k-means objective function therefore we can expect similar results to K-means
* maximum or complete linkage minimizes the maximum distance between observations of pairs of clusters
* average linkage minimizes the average of the distances between all observations of pairs of clusters
* single linkage minimizes the distance between the closest observations of pairs of clusters

Each of the linkage methods were tested on the data with Ward's linkage method returning the best results in all runs.

source: **https://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering**

### SOM

A Self-Organizing Map (SOM) is an unsupervised neural network that is trained using unsupervised learning techniques to produce a low dimensional, discretized representation from the input space of the training samples.

An SOM was implemented - albeit with poor results.

### Evaluation

The following techniques were used to evaluate and compare the resulting models. 

* Silhouette
* Calinski Harabaz Index

#### Silhouette

The Silhouette Score attempts to describe how similar a datapoint is to other datapoints in its cluster, relative to datapoints not in its cluster (this is aggregated over all datapoints to get the score for an overall clustering). It is bounded between -1 and 1. Closer to -1 suggests incorrect clustering, while closer to +1 shows that each cluster is very dense. In the code and logs this metric is used for logging/reporting purposes.

source: **https://scikit-learn.org/stable/modules/generated/sklearn.metrics.silhouette_score.html?highlight=silhouette#sklearn.metrics.silhouette_score**

#### Calinski Harabaz Index

The Calinski Harabaz Index is the ratio of the variance of a datapoint compared to points in other clusters, against the variance compared to points within its cluster. A high score is desirable. This metric is used to choose the best model in this application.

source: **https://scikit-learn.org/stable/modules/generated/sklearn.metrics.calinski_harabasz_score.html**

## Implementation

The following steps are conducted within the code:

* read and process data
* create features
* normalise and center data
* apply and compare models
	* K-means
	* agglomerative clustering
	* Self-Organizing Maps (SOM)
* choose the best model based on the Calinski Harabaz Index
* log all results
* write the output to file

### Runing the model:

Run the model with the below command:

```python main.py```

The python script will ingest the input data from within the same folder and output the results as a csv file named 'results.csv'. The script will also produce a log file with the runtime details. The log file will be named in the format log_YYYYDDMM.

### Results

The best approach found was Agglomerative Clustering, clustering with a malware proportion 1.1%. This is above the expected class distribution, if false positives are acceptable this might be deemed an acceptable results if using an evaluation metric such as recall, but it would be a poor result if using percision. The Agglomerative Clustering algorithim identified 62 malware classes. The other algorithms identified just one!

The results file contains a column thats indicates malware in the 'malware' column.

## Future Work

Given more time, I would have implemented a custom Pipeline that can be optimised. Optimising the parameters for each step in which each step would be:

* Feature selection
* Feature engineering
* Modelling

With optimisation and an appropiate search space result in better results

I would have prefered to write a wrapper for each step, a genric set of classes that allows an auto ML approach!

<hr>
